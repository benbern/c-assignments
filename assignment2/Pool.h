//
// Created by ben on 1/18/16.
//
/*
 * SUMMARY OF THINGS I LEARNED WHILE MAKING THIS
 *
 * I learned that reinterpret_cast, itself, returns only an R-value
 * so you can't do something like reinterpret_cast<char*>(ptr) = &c;
 * but you can do something like *reinterpet_cast<char**>(ptr) = &c;
 * or new (reinterpret_cast<char*>(ptr)) char*{&c};
 *
 * I also learned to use placement new and I learned about one way to
 * make a memory pool in C++
 *
 * I also learned that linked lists can manifest themselves in different
 * mediums, like this one where you "embed" a memory address at a memory
 * region pointed to by a char*, even though its size is much larger than
 * a char's size
 */
#ifndef ASSIGNMENT2_POOL_H
#define ASSIGNMENT2_POOL_H


#include <new>      //for placement new
#include <cstddef>  //for size_t
#include <iostream> //for std::cout and std::endl
#include <cassert>  //for assert

class Pool {
private:
    char ** pool = nullptr;
    char * next = nullptr;
    size_t blocks;
    size_t elemSize;
    size_t blockSize;

    Pool(const Pool&) = delete;
    Pool(Pool&&) = delete;
    Pool& operator=(const Pool&) = delete;
    Pool& operator=(Pool&&) = delete;

    static void * operator new(size_t) noexcept;

public:

    /**
     * Pool Ctor
     * @brief: constructs a Pool object
     */
    Pool(size_t elemSize, size_t blockSize = 5);

    /**
     * Pool Dtor
     * @brief: destroys the pool
     *
     */
    ~Pool();

    /**
     * allocate
     * @brief: gets address to available heap data and creates more
     * heap space if needed
     *
     * @return: a void pointer representing the address space for an
     * object to reside on pool's heap
     */
    void* allocate();


    /**
     * deallocate
     * @brief: free's an object's slot (pushes the address on the "free list")
     * @param void* a void pointer representing the memory address for the object to be freed
     */
    void deallocate(void*);

};


#endif //ASSIGNMENT2_POOL_H
