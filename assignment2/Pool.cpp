//
// Created by ben on 1/18/16.
//

#include "Pool.h"


Pool::Pool( size_t elemSize, size_t blockSize ) :
        blocks{ 0 },
        elemSize{ elemSize },
        blockSize{ blockSize }
{
    assert( elemSize >= sizeof( char* ) );
    std::cout << "Initializing pool with element size " << elemSize
    << " and block size " << blockSize << std::endl;
}

void* Pool::allocate() {

    if( next == nullptr ) {
        std::cout << "Expanding pool..." << std::endl;

        //create a new pool with one more block
        char ** new_pool = new char * [ blocks + 1 ];

        //copy over address from old pool's blocks
        std::copy( pool, pool + blocks, new_pool );

        delete [] pool;

        pool = new_pool;

        //make a new block
        pool[ blocks ] = new char[ blockSize * elemSize ];

        std::cout << "Linking cells starting at: "
        << static_cast< void* >( pool[ blocks ] ) << std::endl;
        for( size_t i = 0; i < ( blockSize - 1 ) ; ++i ) {//uses placement new
            new( pool[ blocks ] + ( i * elemSize ) )
                    char * { pool[ blocks ] + (( i + 1 ) * elemSize ) };
        }//end for loop

        //set final block element to nullptr
        new ( pool[ blocks ] + blockSize * elemSize ) char * { nullptr }; // works on clang++-3.7

        //set next to the start of the newest block
        next = pool[ blocks ];

        ++blocks;
    }

    char * place_for_object = next;

    //using reinterpret cast to 'peek'
    next = *reinterpret_cast< char** >( next );

    std::cout << "Cell allocated at "
    << static_cast< void* >( place_for_object ) << std::endl;

    assert( place_for_object != next );

    return place_for_object;
}

void Pool::deallocate(void * pointer_to_deallocated_obj) {
    // set the address of the freed object to 'point' to
    // what is currently 'next'
    new ( pointer_to_deallocated_obj ) char * { next };

    //set 'next' to the most recently freed address
    next = static_cast< char* >( pointer_to_deallocated_obj );


    std::cout << "Cell deallocated at "
    << static_cast< void* >( pointer_to_deallocated_obj ) << std::endl;
}

Pool::~Pool(){
    std::cout << "Deleting " << blocks << " blocks" << std::endl;

    while( blocks-- )
        delete [] pool[ blocks ];

    delete [] pool;
}

void *Pool::operator new(size_t t) noexcept {
    return nullptr;//don't use!
}
