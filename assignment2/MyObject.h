//
// Created by ben on 1/18/16.
//

#ifndef ASSIGNMENT2_MYOBJECT_H
#define ASSIGNMENT2_MYOBJECT_H

#include<string>
#include<iostream>
#include"Pool.h"


class MyObject {

private:
    std::string name;
    int id;
    static Pool pool;

    MyObject(const MyObject&) = delete;
    MyObject(MyObject&&) = delete;
    MyObject& operator=(const MyObject&) = delete;
    MyObject& operator=(MyObject&&) = delete;

    MyObject(int,const std::string&);
    static void* operator new(size_t);

public:
    static MyObject* create( int, const std::string& );
    friend std::ostream& operator<<(std::ostream&, const MyObject&);
    static void operator delete(void*);
};


#endif //ASSIGNMENT2_MYOBJECT_H
