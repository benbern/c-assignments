//
// Created by ben on 1/18/16.
//

#include "MyObject.h"

Pool MyObject::pool{ sizeof(MyObject) };


//MyObject::MyObject() : name{nm}, id{i}{}
MyObject::MyObject(int i, const std::string &nm): name{nm},id{i} { }


MyObject* MyObject::create(int id, const std::string& name) {
    return new MyObject(id,name);
}

void MyObject::operator delete( void* myObj ) {
    pool.deallocate( myObj );
}

std::ostream& operator<<(std::ostream& os, const MyObject& myobj){
    return os << '{' << myobj.id << ',' << myobj.name << '}';
}

void *MyObject::operator new(size_t size)  {
    return pool.allocate();
}


