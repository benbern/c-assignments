#include <atomic>
#include <cassert>
#include <condition_variable>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
using namespace std;

class Pipeline {
    static queue<int> random_ints, filtered_nums;
    static condition_variable q_cond;
    static mutex q_sync, print;
    static atomic_size_t nprod, nfilt;
public:
    static const size_t total_ints = 5000, nprods = 5, nfilts = 4, ngroupers = 11;
    static const size_t ints_per_thread = total_ints / nprods;
    enum ARBITRARY_INTS{ FILTER_NUM_0 = 3, FILTER_NUM_1 = 13};
    static void filter() {
        for (;;)
        {
            // Get lock for sync mutex
            unique_lock<mutex> qlck(q_sync);

            // Wait for queue to have something to process
            q_cond.wait(qlck,[](){return !random_ints.empty() || !nprod;});
            if (random_ints.empty()) {
                assert(!nprod);
                break;
            }
            auto x = random_ints.front();
            random_ints.pop();
            qlck.unlock();

            if((x % FILTER_NUM_0 != 0) || (x % FILTER_NUM_1 != 0)){
                unique_lock<mutex> filt_lck(q_sync);
                filtered_nums.push(x);
                filt_lck.unlock();
            }
        }
        --nfilt;
    }

    static void group(int i)
    {
        size_t count{0};
        std::string s = "grp" + to_string(i) + ".txt";
        ofstream f(s);
        for(;;){

            unique_lock<mutex> filt_lck(q_sync);
            if (filtered_nums.empty())
            {
                assert(!nfilt);
                filt_lck.unlock();
                break;
            }

            auto val = filtered_nums.front();

            if(val % ngroupers == i)
            {
                filtered_nums.pop();
                filt_lck.unlock();
                f << val << endl;
                ++count;
            }
        }
        lock_guard<mutex> plck(print);
        cout << "Group " << i << " has " << count << " numbers\n";
    }

    static void produce(int i) {
        srand(time(nullptr)+i*(i+1));
        for (int i = 0; i < ints_per_thread; ++i)
        {
            int n = rand();

            // Get lock for queue; push int
            unique_lock<mutex> slck(q_sync);
            random_ints.push(n);
            slck.unlock();
            q_cond.notify_one();
        }

        // Notify consumers that a producer has shut down
        --nprod;
        q_cond.notify_all();
    }
};

queue<int> Pipeline::random_ints, Pipeline::filtered_nums;
condition_variable Pipeline::q_cond;
mutex Pipeline::q_sync, Pipeline::print;
atomic_size_t Pipeline::nprod(nprods), Pipeline::nfilt(nfilts);

int main() {
    vector<thread> prods, filterers, groupers;
    for (size_t i = 0; i < Pipeline::nfilts; ++i)
        filterers.push_back(thread(&Pipeline::filter));
    for (size_t i = 0; i < Pipeline::nprods; ++i)
        prods.push_back(thread(&Pipeline::produce,std::move(i)));
    for (size_t i = 0; i < Pipeline::ngroupers; ++i)
        groupers.push_back(thread(&Pipeline::group, std::move(i)));


    // Join all threads
    for (auto &p: prods)
        p.join();
    for (auto &f: filterers)
        f.join();
    for (auto &g: groupers)
        g.join();
}