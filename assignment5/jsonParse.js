var fs = require('fs');

var words = fs.readFileSync('./words.json','utf-8');
var thingy = JSON.parse(words);

thingy.filter(obj => obj['symbol'] || obj['symbols'])
  .forEach(thing => {
    if(thing.symbol)
      console.log("symbol " + thing.symbol);
    else
      console.log("symbols " + thing.symbols);
  });
