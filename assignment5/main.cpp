#include <iostream>
#include <regex>
#include <string>
#include <map>
#include <fstream>
#include <set>

using namespace std;
auto comp0 = [](const string& s1,const string& s2){ return s1 < s2;};
map<string,size_t,decltype(comp0)> globalWordcountMap(comp0);
string makeStringLowerCase(string & s){ transform(begin(s),end(s),begin(s),::tolower);return s; }

map<string,string> getJSONObjMapFromMatch(const smatch& regex_match){
    static regex line_reg("\\\"(line)\\\"\\.*:\\.*(\\d+),?");
    static regex endline_reg("\\\"(endline)\\\"\\s*:\\s*(\\w+),?");
    static regex position_reg("\\\"(position)\\\"\\s*:\\s*(\\d+),?");
    static regex symbols_reg("\\\"(symbols?)\\\"\\s*:\\s*\\\"(.*)\\\",?");
    static regex word_reg("\\\"?(word)\\\"?\\s*:\\s*\\\"(\\w+)\\\",?");
    map<string,string> wordVals;
    string word, temp = regex_match.str();
    smatch results;
    if(regex_search(temp,results,line_reg)) {
        wordVals[results[1].str()] = results[2].str();
    }
    if(regex_search(temp,results,word_reg)) {
        word = results[2].str();
        word = makeStringLowerCase(word);
        wordVals[results[1].str()] = results[2].str();
    }
    if(regex_search(temp,results,position_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,endline_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,symbols_reg))
        wordVals[results[1].str()] = results[2].str();

    if(word.size())
        ++globalWordcountMap[word];

    return wordVals;
}

int main(int argc,char ** argv) {
    string json_str;
    ifstream lil_file;
    if(argc == 2){
        lil_file.open(argv[1]);
        json_str.assign(istreambuf_iterator<char>{lil_file},{});
    }
    else
        json_str.assign(istreambuf_iterator<char>{cin},{});

    auto comp1 = [](map<string,string> map1,map<string,string> map2)->bool{
        if (stoi(map1["line"]) == stoi(map2["line"])) return stoi(map1["position"]) < stoi(map2["position"]);
        return stoi(map1["line"]) < stoi(map2["line"]);
    };
    set<map<string,string>,decltype(comp1) > mapped_jsons(comp1);

    regex json_reg("\\{([^}}]*)\\},?");
    transform(sregex_iterator(begin(json_str),end(json_str),json_reg),sregex_iterator(),inserter(mapped_jsons,begin(mapped_jsons)),getJSONObjMapFromMatch);

    for(auto & p : globalWordcountMap)
        cout << p.first << " : " << p.second << endl;

    cout << endl;

    bool change_line = false;
    size_t prev_line = 1;
    for(auto & m : mapped_jsons){
        bool offset_first_line = true;
        if(change_line){
            auto line_ptr = m.find("line");
            auto line = stol(line_ptr->second);
            auto line_diff = line - prev_line;
            while(line_diff--)
                cout.put('\n');

            change_line = false;
            prev_line = line;
            offset_first_line = false;
        }
        for(auto & p : m){
            if(p.first == "word") {
                if(offset_first_line)
                    cout << " " << p.second;
                else {
                    cout << p.second;
                    offset_first_line = true;
                }
            }
            else if(p.first == "symbol" || p.first == "symbols")
                cout << p.second;
            else if(p.first == "endline")
                change_line = true;
        }
    }
    return 0;
}