#include <iostream>
#include <regex>
#include <string>
#include <map>
#include <fstream>
#include <set>

using namespace std;
auto comp0 = [](const string& s1,const string& s2){ return s1 < s2;};
map<string,size_t,decltype(comp0)> globalWordcountMap(comp0);

string makeStringLowerCase(string & s){
    transform(begin(s),end(s),begin(s),::tolower);
    return s;
}

map<string,string> getJSONObjMap(istream& in){
    static regex line_reg("\\\"(line)\\\"\\.*:\\.*(\\d+),?");
    static regex endline_reg("\\\"(endline)\\\"\\s*:\\s*(\\w+),?");
    static regex position_reg("\\\"(position)\\\"\\s*:\\s*(\\d+),?");
    static regex symbols_reg("\\\"(symbols?)\\\"\\s*:\\s*\\\"(.*)\\\",?");
    static regex word_reg("\\\"?(word)\\\"?\\s*:\\s*\\\"(\\w+)\\\",?");
    map<string,string> wordVals;
    in.ignore(numeric_limits<streamsize>::max(),'{');
    string temp,word;
    getline(in,temp,'}');
    smatch results;
    if(regex_search(temp,results,line_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,word_reg)) {
        word = results[2].str();
        word = makeStringLowerCase(word);
        wordVals[results[1].str()] = results[2].str();
    }
    if(regex_search(temp,results,position_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,endline_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,symbols_reg))
        wordVals[results[1].str()] = results[2].str();

    if(word.size()) {
        ++globalWordcountMap[word];
    }

    return wordVals;
}

map<string,string> getJSONObjMapFromStr(string& temp){
    static regex line_reg("\\\"(line)\\\"\\.*:\\.*(\\d+),?");
    static regex endline_reg("\\\"(endline)\\\"\\s*:\\s*(\\w+),?");
    static regex position_reg("\\\"(position)\\\"\\s*:\\s*(\\d+),?");
    static regex symbols_reg("\\\"(symbols?)\\\"\\s*:\\s*\\\"(.*)\\\",?");
    static regex word_reg("\\\"?(word)\\\"?\\s*:\\s*\\\"(\\w+)\\\",?");
    map<string,string> wordVals;
    string word;
    smatch results;
    if(regex_search(temp,results,line_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,word_reg)) {
        word = results[2].str();
        word = makeStringLowerCase(word);
        wordVals[results[1].str()] = results[2].str();
    }
    if(regex_search(temp,results,position_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,endline_reg))
        wordVals[results[1].str()] = results[2].str();
    if(regex_search(temp,results,symbols_reg))
        wordVals[results[1].str()] = results[2].str();

    if(word.size()) {
        ++globalWordcountMap[word];
    }

    return wordVals;
}

int main(int argc,char ** argv) {
    try {
        regex lol_wut("\\{([^}}]*)\\},?");
    }catch(regex_error & e){
        cout << e.what() << endl;
        cout << e.code() << endl;
        cout << regex_constants::error_paren << endl;
        exit(1);
    }


    ifstream lil_file(argv[1]);
    string fucking_json(istream_iterator<char>(lil_file),(istream_iterator<char>()));

    auto comp1 = [](map<string,string> map1,map<string,string> map2)->bool{
        if (stoi(map1["line"]) == stoi(map2["line"])) return stoi(map1["position"]) < stoi(map2["position"]);
        return stoi(map1["line"]) < stoi(map2["line"]);
    };

    set<map<string,string>, decltype(comp1) > json_objs(comp1);
    ifstream infile(argv[1]);
    while(infile) {
        auto temp_map = getJSONObjMap(infile);
        if(!temp_map.empty())
            json_objs.insert(temp_map);
    }

    for(auto & p : globalWordcountMap){
        //cout << p.first << " : " << p.second << endl;
    }

    bool change_line = false;
    size_t prev_line = 1;
    for(auto & m : json_objs){
        size_t current_line = 1;
        if(change_line){
            auto line_ptr = m.find("line");
            auto line = stol(line_ptr->second);
            auto line_diff = line - prev_line;
            while(line_diff--)
                cout.put('\n');

            change_line = false;
            prev_line = line;
        }
        for(auto & p : m){
            if(p.first == "word")
                cout << " " << p.second ;
            else if(p.first == "symbol" || p.first == "symbols")
                cout << p.second;
            else if(p.first == "endline")
                change_line = true;
        }
    }



    return 0;
}