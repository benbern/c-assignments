#include<iostream>
#include<regex>
#include<string>
#include<set>
#include<map>
#include<fstream>

/*
 * I learned about regular expressions when doing this assignment.
 */

using namespace std;
/* i made this b/c strcasecmp is NOT cross platform */
static string makeStringLowerCase(string s){ transform(begin(s),end(s),begin(s),::tolower); return s; }//strcasecmp doesn't work on windows
auto globalComp = [](const string& s1, const string& s2){ return makeStringLowerCase(s1) < makeStringLowerCase(s2); };
map<string,size_t,decltype(globalComp)> globalWordCountMap(globalComp);//GLOBAL container as per *literal* specs

//This function converts a regular expression that has matched a json object to a map strings to their properties
map<string,string> matchToJsonMap(const smatch& match) {
    map<string,string> jsonValMap;
    auto match_str = match.str();

    static regex value_parser("\\\"(\\w+)\\\":\\\"?(\\w+|\\\\\"|//|[\\W+\\\"])\\\"?,?");
    transform(sregex_iterator(begin(match_str),end(match_str),value_parser),sregex_iterator(),inserter(jsonValMap,begin(jsonValMap)),
              [&match_str](const smatch& lil_match)->pair<string,string> {
                  static regex the_actual_word_word("word");
                  if(regex_match(lil_match[1].str(),the_actual_word_word))//FROM THE SPECS: Use a *global* container associating each word (not symbols) in the document with a total number of times it appears
                      ++globalWordCountMap[lil_match[2].str()];

                  static regex the_actual_word_symbol("symbols?");
                  if(regex_match(lil_match[1].str(),the_actual_word_symbol)) {
                      static regex symbols_reg("\\\"(symbols?)\\\":\\\"([^\"]*|\\\\\")\\\"");
                      smatch symbols_result;
                      regex_search(match_str,symbols_result,symbols_reg);
                      return make_pair(symbols_result[1].str(),symbols_result[2].str());
                  }
                  return make_pair(lil_match[1].str(),lil_match[2].str());
              });
    return jsonValMap;
}

int main(int argc, char ** argv){
    string json_str;
    ifstream in_file;
    if(argc == 2) {
        in_file.open(argv[1]);
        json_str.assign(istream_iterator<char>{in_file},{});
    }else
        json_str.assign(istream_iterator<char>{cin},{});

    auto comp1 = [](map<string,string> map1,map<string,string> map2){ return (stoi(map1["line"]) == stoi(map2["line"]))? stoi(map1["position"]) < stoi(map2["position"]) : stoi(map1["line"]) < stoi(map2["line"]); };
    set<map<string,string>,decltype(comp1) > mapped_jsons(comp1);

    static regex json_reg("\\{([^}]*\\\"}\\\"[^}]*|[^}]*)\\}");
    transform(sregex_iterator(begin(json_str),end(json_str),json_reg),sregex_iterator(),
              inserter(mapped_jsons,begin(mapped_jsons)),matchToJsonMap);

    for(auto & p : globalWordCountMap)//FROM THE SPECS:  Print the word and count delimited by a tab.  Iterate through the container and...
        cout << p.first << "\t" << p.second << endl;

    cout << endl;//FROM THE SPECS:delimit by a new line.

    bool change_line = false;
    size_t current_line = 1;
    for(auto & m : mapped_jsons){
        if(change_line){
            auto line_ptr = m.find("line");
            auto next_line = stol(line_ptr->second);
            auto line_diff = next_line - current_line;
            while(line_diff--) cout.put('\n');
            change_line = false;
            current_line = next_line;
        }
        for(auto & p : m){
            static regex back_slash("\\\\");
            cout << (p.first == "word" ? " " + p.second : (p.first == "symbol" || p.first == "symbols") ? regex_replace(p.second,back_slash,"") :"");
            if(p.first == "endline") change_line = true;
        }
    }
    return 0;
}