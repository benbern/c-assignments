//
// Created by ben on 2/13/16.
//

#ifndef ASSIGNMENT3_EMPLOYEE_H
#define ASSIGNMENT3_EMPLOYEE_H


#include<iostream>
#include<stdexcept>
#include<cstring>
#include <limits>
#include <algorithm>
#include <cassert>

class Employee {
    //constants
    static const int BITSET_SZ = 9;
    static const int NAME_SIZE = 31;
    static const int ADDRESS_SIZE = 26;
    static const int CITY_SIZE = 21;
    static const int STATE_SIZE = 21;
    static const int COUNTRY_SIZE = 21;
    static const int PHONE_SIZE = 21;
    const char * const EMP_OPEN = "<Employee>";
    const char * const EMP_CLOSE = "</Employee>";
    const char * const NAME_OPEN = "<Name>";
    const char * const NAME_CLOSE = "</Name>";
    const char * const ID_OPEN = "<ID>";
    const char * const ID_CLOSE = "</ID>";
    const char * const ADDR_OPEN = "<Address>";
    const char * const ADDR_CLOSE = "</Address>";
    const char * const CITY_OPEN = "<City>";
    const char * const CITY_CLOSE = "</City>";
    const char * const STATE_OPEN = "<State>";
    const char * const STATE_CLOSE = "</State>";
    const char * const COUNTRY_OPEN = "<Country>";
    const char * const COUNTRY_CLOSE = "</Country>";
    const char * const PHONE_OPEN = "<Phone>";
    const char * const PHONE_CLOSE = "</Phone>";
    const char * const SALARY_OPEN = "<Salary>";
    const char * const SALARY_CLOSE = "</Salary>";
    static const char START_DELIM = '<';
    static const char END_DELIM = '>';
    enum FLAG{NAME = 1, ID, ADDRESS, CITY, STATE,COUNTRY,PHONE,SALARY };


//setters and getters (my IDE made these)
public:
    const std::string &getAddress() const { return address; }
    void setAddress(const std::string &address) { this->address = address; }
    double getSalary() const { return salary; }
    void setSalary(double salary) { this->salary = salary; }
    const std::string &getPhone() const { return phone; }
    void setPhone(const std::string &phone) { this->phone = phone; }
    const std::string &getCountry() const { return country; }
    void setCountry(const std::string &country) { this->country = country; }
    const std::string &getState() const { return state; }
    void setState(const std::string &state) { this->state = state; }
    const std::string &getCity() const { return city; }
    void setCity(const std::string &city) { this->city = city; }
    int getId() const { return id; }
    void setId(int id) { this->id = id; }
    const std::string &getName() const { return name; }
    void setName(const std::string &name) { this->name = name; }

private:
    std::string name;
    int id;
    std::string address;
    std::string city;
    std::string state;
    std::string country;
    std::string phone;
    double salary;

    struct EmployeeRec {
        int id;
        char name[NAME_SIZE];
        char address[ADDRESS_SIZE];
        char city[CITY_SIZE];
        char state[STATE_SIZE];
        char country[COUNTRY_SIZE];
        char phone[PHONE_SIZE];
        double salary;
    };

public:
    Employee(const std::string& nm,
             int id,
             const std::string& addrss,
             const std::string& cty,
             const std::string& st,
             const std::string& cntry,
             const std::string& phn,
             double slry = 0.0);

    void display(std::ostream&) const;  // Write a readable Employee representation to a stream
    void write(std::ostream&) const;    // Write a fixed-length record to current file position
    void store(std::iostream&) const;   // Overwrite (or append) record in (to) file
    void toXML(std::ostream&) const;    // Write XML record for Employee
    static Employee* read(std::istream&);         // Read record from current file position
    static Employee* retrieve(std::istream&,int); // Search file for record by id
    static Employee* fromXML(std::istream&);      // Read the XML record from a stream

    /*private utility functions*/
    static std::string getNextTag(std::istream&);
};
#endif //ASSIGNMENT3_EMPLOYEE_H
