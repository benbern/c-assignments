
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <cctype>
#include <assert.h>
#include "Employee.h"

//using namespace std;

int main(int argc, char ** argv) {
    if(argc == 1){
        //1) Obtain the name of an XML file to read from the command line (argv[1]).
        // Print an error message and halt the program if there is no command-line argument provided,
        std::cout << "Error: Please enter a file name.\n Usage: ./a.out FILENAME\n";
        exit(1);
    }

    std::vector<std::unique_ptr<Employee> > v;

    try {
        std::ifstream fin(argv[1]);
        //Print an error if the file does not exist.
        if(!fin.good()){
            std::cout << "Error: could not open file: "
            << argv[1] << std::endl;
            exit(1);
        }
        //2) Read each XML record in the file by repeatedly calling Employee::fromXML,
        // which creates an Employee object on-the-fly,
        // and store it in a vector of Employee pointers (you may use smart pointers if you wish).
        while(fin.good()) {
            std::unique_ptr<Employee> temp{Employee::fromXML(fin)};
            v.push_back(std::move(temp));
        }
        for(std::unique_ptr<Employee>& e : v) {
            e->display(std::cout);
            std::cout << std::endl;
        }
    }
    catch (std::runtime_error& err){
        std::cout << err.what() << std::endl;
        exit(1);
    }

    try {
        std::fstream f("employee.bin",
                       std::ios::in | std::ios::out | std::ios::binary | std::ios::trunc);

        for(std::unique_ptr<Employee>& e : v)
            e->write(f);

        v.clear();
        f.clear();

        f.seekg(0,std::ios::beg);

        while(f.good()) {
            Employee * employee_ptr = Employee::read(f);
            if(employee_ptr != nullptr) {
                std::unique_ptr<Employee> temp{employee_ptr};
                v.push_back(std::move(temp));
            }
        }

        f.clear();
        f.seekg(0,std::ios::beg);


        for(std::unique_ptr<Employee>& e : v) {
            e->toXML(std::cout);
            std::cout << std::endl << std::endl;
        }

        f.seekg(0,f.beg);

        //8) Search the file for the Employee with id 12345 using Employee::retrieve.
        std::unique_ptr<Employee> employee_12345 { Employee::retrieve(f,12345) };
        if(employee_12345 != nullptr) {//Employee retrieve returns a nullptr if the employee is not found
            std::cout << "Found:" << std::endl;
            employee_12345->display(std::cout);
            //9) Change the salary for the retrieved object to 150000.0
            employee_12345->setSalary(150000.0);
        }

        f.seekg(0,f.beg);

        //10) Write the modified Employee object back to file using Employee::store
        employee_12345->store(f);

        f.seekg(0,f.beg);

        //11) Retrieve the object again by id (12345) and print its salary to verify that the file now has the updated salary.
        std::unique_ptr<Employee> employee_12345_dup { Employee::retrieve(f,12345) };
        if(employee_12345_dup != nullptr) {
            std::cout << employee_12345->getSalary() << std::endl << std::endl;
        }

        f.seekg(0,f.beg);

        //12) Create a new Employee object of your own with a new, unique id, along with other information.
        Employee homerSimpson("Homer Simpson",
                            90210,
                            "123 fake st",
                            "Springfield",
                            "OH",
                            "USA",
                            "911",
                            2.00);

        //13) Store it in the file using Employee::store.
        homerSimpson.store(f);

        f.seekg(0,f.beg);

        //14) Retrieve the record with Employee::retrieve and display it to cout.
        std::unique_ptr<Employee> homer_simpson_ptr { Employee::retrieve(f,90210) };
        if(homer_simpson_ptr != nullptr) {
            std::cout << "Found:" << std::endl;
            homer_simpson_ptr->display(std::cout);
        }


    }
    catch(std::runtime_error& error){
        std::cout << error.what() << std::endl;
    }

    return 0;
}