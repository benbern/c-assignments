//
// Created by ben on 2/13/16.
//
#include <iomanip>
#include <bitset>
#include "Employee.h"


/*
 * Summary of things I have learned:
 *
 * I learned a lot about IO. I learned that I should probably start my projects earlier
 * so I don't write functions spaghetti code-ish like how I wrote Employee::fromXML
 */

Employee::Employee(const std::string &nm, int id, const std::string& addrss ,const std::string &cty, const std::string &st,
                   const std::string &cntry, const std::string &phn, double slry) :
        name{nm},
        id{id},
        address{addrss},
        city{cty},
        state{st},
        country{cntry},
        phone{phn},
        salary{slry}
{}


Employee *Employee::fromXML(std::istream &in) {
    std::string name{};
    int id{};
    std::string address{};
    std::string city{};
    std::string state{};
    std::string country{};
    std::string phone{};
    double salary{};
    std::bitset<BITSET_SZ> bs;

    std::string first_emp_tag{ getNextTag(in) };

    if(strcasecmp(first_emp_tag.c_str(),"employee") == 0){
       bs.set(0);
    }
    else{
        throw std::runtime_error("Missing <Employee> tag");
    }

    while(in.good())
    {
        std::string inner_tag_1 = getNextTag(in);

        if (strcasecmp(inner_tag_1.c_str(),"/employee") == 0) {
            if(name.size() == 0){
                throw std::runtime_error("Missing <Name> tag");

            }
            if(id == 0){
                throw std::runtime_error("Missing <ID> tag");
            }
            int c = static_cast<int>(in.peek());//in.peek() returns an unsigned long on Debian GNU/Linux x86_64 using clang++-3.7
            //move past all whitespace
            while(isspace(c)){
                in.get();
                c = static_cast<int>(in.peek());
            }
            break;
        }

        auto value_pos = in.tellg();
        std::string inner_tag_2 = getNextTag(in);

        if (strcasecmp(inner_tag_1.c_str(), inner_tag_2.substr(1, inner_tag_2.size()).c_str()) == 0) {
            //get value
            in.seekg(value_pos, std::ios::beg);
            std::string value;
            getline(in, value, START_DELIM);

            if(strcasecmp(inner_tag_1.c_str(),"name") == 0){
                if(bs[FLAG::NAME])
                    throw std::runtime_error("Multiple <Name> tags");

                name = value;
                bs.set(FLAG::NAME);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"id") == 0){
                if(bs[FLAG::ID])
                    throw std::runtime_error("Multiple <ID> tags");

                id = stoi(value);
                bs.set(FLAG::ID);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"address") == 0){
                if(bs[FLAG::ADDRESS])
                    throw std::runtime_error("Multiple <Address> tags");

                address = value;
                bs.set(FLAG::ADDRESS);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"city") == 0){
                if(bs[FLAG::CITY])
                    throw std::runtime_error("Multiple <City> tags");

                city = value;
                bs.set(FLAG::CITY);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"state") == 0){
                if(bs[FLAG::STATE])
                    throw std::runtime_error("Multiple <State> tags");

                state = value;
                bs.set(FLAG::STATE);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"country") == 0){
                if(bs[FLAG::COUNTRY])
                    throw std::runtime_error("Multiple <Country> tags");

                country = value;

                bs.set(FLAG::COUNTRY);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"phone") == 0){
                if(bs[FLAG::PHONE])
                    throw std::runtime_error("Multiple <Phone> tags");

                phone = value;
                bs.set(FLAG::PHONE);
            }
            else if(strcasecmp(inner_tag_1.c_str(),"salary") == 0){
                if(bs[FLAG::SALARY])
                    throw std::runtime_error("Multiple <Salary> tags");

                salary = stod(value);
                bs.set(FLAG::SALARY);
            }
            else{
                throw std::runtime_error("Invalid tag: <" + inner_tag_1 + ">");
            }

            in.ignore(std::numeric_limits<std::streamsize>::max(), END_DELIM);
        }
        else {
            throw std::runtime_error("Missing </" + inner_tag_1 + "> tag");
        }
    }
    return new Employee(name,id,address,city,state,country,phone,salary);
}

std::string Employee::getNextTag(std::istream &in) {
    //move past the first tag
    in.ignore(std::numeric_limits<std::streamsize>::max(),'<');
    //get tag string, but ignore '>'
    std::string temp;
    getline(in,temp,END_DELIM);
    return temp;
}

void Employee::display(std::ostream & out) const {
    out << "id: " << id << std::endl;
    out << "name: " << name << std::endl;
    out << "address: " << address << std::endl;
    out << "city: " << city << std::endl;
    out << "state: " << state << std::endl;
    out << "country: " << country << std::endl;
    out << "phone: " << phone << std::endl;
    out << "salary: " << salary << std::endl;
}

void Employee::write(std::ostream & out) const {
    EmployeeRec record;
    record.id = id;
    strncpy(record.name,name.c_str(),NAME_SIZE-1)[NAME_SIZE-1]='\0';
    strncpy(record.address,address.c_str(),ADDRESS_SIZE-1)[ADDRESS_SIZE-1]='\0';
    strncpy(record.city,city.c_str(),CITY_SIZE-1)[CITY_SIZE-1]='\0';
    strncpy(record.state,state.c_str(),STATE_SIZE-1)[STATE_SIZE-1]='\0';
    strncpy(record.country,country.c_str(),COUNTRY_SIZE-1)[COUNTRY_SIZE-1]='\0';
    strncpy(record.phone,phone.c_str(),PHONE_SIZE-1)[PHONE_SIZE-1]='\0';
    record.salary = salary;
    out.write(reinterpret_cast<char*>(&record),sizeof(record));
}

Employee * Employee::read(std::istream & in) {
    EmployeeRec buffer;
    in.read(reinterpret_cast<char *>(&buffer), sizeof(buffer));

    if(in){
        return new Employee(buffer.name,
                            buffer.id,
                            buffer.address,
                            buffer.city,
                            buffer.state,
                            buffer.country,
                            buffer.phone,
                            buffer.salary);
    }else {
        return nullptr;
    }
}

Employee * Employee::retrieve(std::istream & in, int target_id) {
    EmployeeRec buffer;
    buffer.id = 0;
    while(buffer.id != target_id && in) {
        in.read(reinterpret_cast<char *>(&buffer), sizeof(buffer));
    }

    if(in){
        return new Employee(buffer.name,
                            buffer.id,
                            buffer.address,
                            buffer.city,
                            buffer.state,
                            buffer.country,
                            buffer.phone,
                            buffer.salary);
    }
    else{
        return nullptr;
    }
}


void Employee::toXML(std::ostream & out) const {
    out << EMP_OPEN << std::endl;
    out << "    " << NAME_OPEN << name << NAME_CLOSE << std::endl;
    out << "    " << ID_OPEN << id << ID_CLOSE << std::endl;
    if(address.size()){
        out << "    " << ADDR_OPEN << address << ADDR_CLOSE << std::endl;
    }
    if(city.size()){
        out << "    " << CITY_OPEN << city << CITY_CLOSE << std::endl;
    }
    if(state.size()) {
        out << "    " << STATE_OPEN << state << STATE_CLOSE << std::endl;
    }
    if(country.size()) {
        out << "    " << COUNTRY_OPEN << country << COUNTRY_CLOSE << std::endl;
    }
    if(phone.size()){
        out << "    " << PHONE_OPEN << phone << PHONE_CLOSE << std::endl;
    }
    if(salary){
        out << "    " << SALARY_OPEN << salary << SALARY_CLOSE << std::endl;
    }
    out << EMP_CLOSE;
}

void Employee::store(std::iostream &io) const {
    assert(id != 0);

    EmployeeRec buffer;
    buffer.id = 0;
    std::ios::pos_type pos;

    io.read(reinterpret_cast<char *>(&buffer), sizeof(buffer));
    while(buffer.id != id && io) {
        pos = io.tellp();
        io.read(reinterpret_cast<char*>(&buffer), sizeof(buffer));
    }
    if(io){
        io.seekp(pos,io.beg);
        this->write(io);
    }
    else{
        io.clear();
        io.seekp(0,io.end);
        this->write(io);
    }
}
