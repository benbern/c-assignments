//
// Created by ben on 4/7/16.
//

#ifndef BITBOARD_TICTACTOE_H
#define BITBOARD_TICTACTOE_H

#include "bitboard.h"

class tictactoe : public bitboard<unsigned short>{
    enum { TTT_ROW_SIZE = 3 , TTT_BOARD_SIZE = 9 };
public:
    tictactoe() = default;
    tictactoe(const tictactoe& ttt);
    tictactoe(const std::string s);
    tictactoe& operator=(const tictactoe& ttt);
    tictactoe& operator=(tictactoe&& ttt);

    std::size_t getSize()  const override;
    std::size_t getRowCount() const override;
};

std::size_t tictactoe::getSize() const
{ return TTT_BOARD_SIZE; }

std::size_t tictactoe::getRowCount() const
{ return TTT_ROW_SIZE; }

tictactoe::tictactoe(const std::string s)
{ this->board = cStrToBoardType(s.c_str()); }

tictactoe::tictactoe(const tictactoe& ttt) : bitboard<unsigned short>(ttt) {}

tictactoe& tictactoe::operator=(const tictactoe& ttt){
    bitboard<unsigned short>::operator=(ttt);
    return *this;
}

tictactoe& tictactoe::operator=(tictactoe&& ttt) {
    bitboard<unsigned short>::operator=(ttt);
    return *this;
}
#endif //BITBOARD_TICTACTOE_H
