//Note to users: if you want this test to use a different base type, change the preprocessor macro here
#define btype unsigned char

#include "bitboard.h"
#include "tictactoe.h"
#include "knight.h"
#include "rook.h"
#include <vector>
#include <map>
#include <cassert>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <typeindex>
#include <regex>

using namespace std;

std::string btos(bitboard<unsigned char> &bb) {
    std::stringstream ss;
    ss.str("");
    ss << bb;
    cout<<ss.str()<<"\n"<<bb<<endl;
    string str = regex_replace(ss.str(),regex("\\s*(.+)\\s*"),"$1");  //corrects any format padding if exists, display is conceptually only for debugging anyway
    return str;
}

std::string btos(bitboard<unsigned char> &&bb) {
    std::stringstream ss;
    ss.str("");
    ss << bb;
    cout<<ss.str()<<"\n"<<bb<<endl;
    string str = regex_replace(ss.str(),regex("\\s*(.+)\\s*"),"$1");  //corrects any format padding if exists, display is conceptually only for debugging anyway
    return str;
}

void test_bitboard() {
    //NOTE TO USERS:
    // don't change the type of these from btype;
    //	if you want to change the template type, change it in the preprocessor macro above
    static btype type_test = 5;

    // TEST Constructors
    bitboard<btype> bb1 = 1u;	//bitboard(T) allow conversion from ‘ = ’
    bb1 = 13u;					//allow conversion from =
    bitboard<btype> bb2 = bb1;	//default copy constructor
    bitboard<btype> bb3(type_test);	//test default constructor
    //test constructors of different types

    {
        bitboard<unsigned char> bb_char(13);
        bitboard<unsigned int> bb_int(13);
        bitboard<uint64_t> bb_long(13);
    }
    cout << "Test different constructors defined in spec" << endl;
    //Test different constructors defined in spec
    {
        std::string string = "x--x-x-x";

        cout << "Testing str constructor.." << endl;
        bb1 = bitboard<btype>(string);

        cout<<btos(bb1).size()<<" : "<< string.size()<<endl;	//bitboard(string) do not allow conversion from ‘ = ’ //(refer to format.doc for format)
        assert(btos(bb1) == string);	//verify that bb1 initialized properly
        //bb1 = string;					//should not compile!
        char* cstring = "x--x-x-x";
        bb1 = bitboard<btype>(cstring);	//bitboard(char*)  do not allow conversion from ‘ = ’ //(refer to format.doc for format)
        assert(btos(bb1) == string);	//verify that bb1 initialized properly
        //bb1 = cstring;	//SHOULD NOT COMPILE
    }

    cout << "Test default assingment operator" << endl;
    {	//default assingment operator
        bb1 = 3;	//0011
        bb2 = 5;	//0101
        bb2 = bb1;
        assert(btos(bb2) == "xx------");	//verify default assignment operator
    }

    cout << "Test a constructor that takes and rvalue type" << endl;
    {	//a constructor that takes and rvalue type
        bb1 = 3;
        bb2 = 5;
        bb3 = bb2 + bb1;
        assert(btos(bb3) == "xxx-----");	//verify a constructor that takes an rvalue type
    }

    //TEST Operators
    {
        //‘operator << ’->prints a representation of the board to a stream(refer to examples in format.doc)
        {
            bb1 = 0xF0;
            assert(btos(bb1) == "----xxxx");
        }
        //‘operator >> ’->reads in stream sets T object to correct value(refer to examples in format.doc)
        {
            std::string string1 = "x--x-x-x";
            std::string string2 = "-x-x\n-X-x";
            std::stringstream ss;
            ss.str(string1);
            //bb1[0] = 0;
            //bb1[1] = 1;
            ss >> bb1;
            ss.str(string2);
            ss.clear();
            ss >> bb1;
        }
        //‘operator + ’->gets result of combining active bits with another board, doesn’t change inner board
        {
            bb1 = 3;	//0011
            bb2 = 5;	//0101
            bb3 = bb1 + bb2;	//0111
            assert(btos(bb1) == "xx------");
            assert(btos(bb2) == "x-x-----");
            assert(btos(bb3) == "xxx-----");
        }
    }
    {
        //TEST Bitwise Functions
        bb1 = 85;	//01010101
        bb1.put(1);
        assert(btos(bb1) == "xxx-x-x-");	//verify that put(i)->activates a bit when it is set to 0
        bb1.put(0);
        assert(btos(bb1) == "xxx-x-x-");	//verify that put(i) doesn't change a bit that is already set to 1
        bb1.erase(1);
        assert(btos(bb1) == "x-x-x-x-");	//verify that erase(i)-deactivates a bit at position i
        bb1.erase(3);
        assert(btos(bb1) == "x-x-x-x-");	//verify that erase(i)-doesn't change a bit that is already set to 0;
        bb1.invert(3);
        assert(btos(bb1) == "x-xxx-x-");	//verify that invert(i) activates a bit at i that was 0
        bb1.invert(4);
        assert(btos(bb1) == "x-xx--x-");	//verify that invert(i) deactivates a bit at i that was 1
        bb1.clear();
        assert(btos(bb1) == "--------");	//verify that clear() deactivates all bits
    }
    {
        //TEST minus and add
        bb1 = 85;
        bb2 = 170u;

        assert(btos(bb1) == "x-x-x-x-");
        assert(btos(bb2) == "-x-x-x-x");

        bb1 += bb2;

        assert(btos(bb1) == "xxxxxxxx");

        bitboard<btype> bb1488 = bb1 - bb2;
        assert(btos(bb1488) == "x-x-x-x-");
        assert(btos(bb1488 - bb1) == "--------");

        cout << endl << endl << endl;

        bitboard<unsigned long long> bb123 = 1uLL;
        cout << bb123;

        for(int x{0}; x < 8; ++x)
            for(int y{0}; y < 8; ++y)
                if((x+y) % 2 == 0)
                    bb123.put(x+y*8);


        cout << endl << endl << endl;
        cout << bb123;

        for(int x{0}; x < 8; ++x)
            for(int y{0}; y < 8; ++y)
                if((x+y) % 2 == 0)
                    bb123.erase(x+y*8);

        cout << endl << endl << endl;
        cout << bb123;

        for(int x{0}; x < 8; ++x)
            for(int y{0}; y < 8; ++y)
                if((x+y) % 2 != 0)
                    bb123.put(x+y*8);


        cout << endl << endl << endl;
        cout << bb123;

        for(int x{0}; x < 8; ++x)
            for(int y{0}; y < 8; ++y)
                if((x+y) % 2 != 0)
                    assert(bb123[x+y*8]);

        vector<bitboard<unsigned long long> > v;

        for(int x{0}; x < 8; ++x)
            for(int y{0}; y < 8; ++y)
                if((x+y) % 2 != 0){
                    bitboard<decltype(1uLL)> bb;
                    bb.put(x+y*8);
                    v.push_back(bb);
                }

        bitboard<decltype(1uLL)> mybb;

        for(auto & bb : v)
           mybb += bb;

        cout << endl << endl;
        cout << mybb;

        tictactoe ttt;

        cout << endl << endl;

        std::cout << ttt;

        ttt.put(0);

        ttt.put(4);

        ttt.put(8);

        std::cout << std::endl << std::endl << std::endl;

        std::cout << ttt;

        tictactoe t4;
        t4 = ttt;

        std::cout << std::endl << std::endl << std::endl;

        tictactoe t3(ttt);

        ttt.put(2);
        ttt.put(6);

        std::cout << std::endl << std::endl << std::endl;

        std::cout << ttt;

        std::stringstream state;

        state << ttt;

        tictactoe xxx(state.str());

        std::cout << std::endl << std::endl;

        std::cout << xxx;

        tictactoe f;

        f = std::move(xxx);

        std::cout << std::endl << std::endl;

        std::cout << f;

        tictactoe homer;
        tictactoe bart;

        cout << "homer == bart == 0\n\n";
        assert(homer == bart);

        homer.put(1);
        homer.put(2);
        homer.put(3);

        homer.put(10);

        cout << "homer == bart == 0\n\n";

        bart.put(1);
        bart.put(2);
        bart.put(3);

        assert(homer == bart);

    }
}

void test_moves(){
    knight k0;
    knight k1("--------\n--------\n--------\n----x---\n--------\n--------\n--------\n--------\n");

    k0 = k1;

    cout << "k0 moves\n";
    auto k0_moves = k0.getAvailableMoves();
    copy(begin(k0_moves),end(k0_moves),ostream_iterator<bitboard<uint64_t> >{cout,"\n\n"});

    //cout << "k1 moves\n";
    //auto moves = k1.getAvailableMoves();
    //copy(begin(moves),end(moves),ostream_iterator<bitboard<uint64_t> >{cout,"\n\n"});


    rook r1("--------\n--------\n--------\n--------\n------x-\n--------\n--------\n--------\n");

    cout << "rq moves\n";
    auto rook_moves = r1.getAvailableMoves();
    copy(begin(rook_moves),end(rook_moves),ostream_iterator<bitboard<uint64_t> >{cout,"\n\n"});

}


int main() {
    //test_bitboard();
    test_moves();
    return 0;
}