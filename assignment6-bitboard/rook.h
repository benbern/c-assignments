//
// Created by ben on 4/7/16.
//

#ifndef BITBOARD_ROOK_H
#define BITBOARD_ROOK_H

#include <bits/stl_map.h>
#include"chesspiece.h"
#include"bitboard.h"
using namespace std;

class rook : public chesspiece, public bitboard<uint64_t> {
private:
    struct chessVector {
        //y'know, like a math vector
        int x;
        int y;

        chessVector(int x, int y) : x{x}, y{y} { }
    };

    bitboard<uint64_t> moveVectorAndPieceVectorToBitBoard(const chessVector &, const chessVector &);

    uint64_t chessVectorToMask(const chessVector &);

public:
    std::vector<bitboard<uint64_t> > getAvailableMoves() override;

    rook() = default;

    rook(const char *);

    rook(const rook &);

    rook &operator=(const rook &r);

    rook &operator=(rook &&);
};

std::vector<bitboard<uint64_t> > rook::getAvailableMoves() {
    std::vector<bitboard<uint64_t> > availableMoves;
    std::vector<chessVector> pieces;
    auto len = bitboard::getRowCount();

    //get position of pieces
    for (int y{0}; y < len; ++y)
        for (int x{0}; x < len; ++x)
            if (operator[](x + y * len))
                pieces.push_back({x, y});

    for (auto &piece : pieces) {
        //get all positive y moves
        for (int y{1};
             piece.y + y < len
             && !operator[](piece.x + (piece.y + y) * len);
             ++y)
            availableMoves.push_back(
                    moveVectorAndPieceVectorToBitBoard({piece.x, piece.y + y}, piece)
            );

        //get all negative y moves
        for (int y{-1};
             piece.y + y >= 0
             && !operator[](piece.x + (piece.y + y) * len);
             --y)
            availableMoves.push_back(
                    moveVectorAndPieceVectorToBitBoard({piece.x, piece.y + y}, piece)
            );

        //get all positive x moves
        for (int x{1};
             piece.x + x < len
             && !operator[](piece.x + x + (piece.y * len));
             ++x)
            availableMoves.push_back(
                    moveVectorAndPieceVectorToBitBoard({piece.x + x, piece.y}, piece)
            );

        //get all negative x moves
        for (int x{-1};
             piece.x + x >= 0
             && !operator[](piece.x + x + (piece.y * len));
             --x)
            availableMoves.push_back(
                    moveVectorAndPieceVectorToBitBoard({piece.x + x, piece.y}, piece)
            );

    }
    return (availableMoves);
}

bitboard<uint64_t> rook::moveVectorAndPieceVectorToBitBoard(const rook::chessVector &tempMove,
                                                            const rook::chessVector &piece) {
    auto tempmask = chessVectorToMask(tempMove);
    auto piecePosMask = chessVectorToMask(piece);
    bitboard<uint64_t> tempBB(tempmask);
    bitboard<uint64_t> pieceBB(piecePosMask);

    return (operator+(tempBB) - pieceBB);
}


uint64_t rook::chessVectorToMask(const rook::chessVector &cv) {
    return (1uLL << (cv.x + cv.y * bitboard::getRowCount()));
}

rook::rook(const char *s) : bitboard<uint64_t>(s) { }

rook::rook(const rook &r) : bitboard<uint64_t>(r.board) { }

rook &rook::operator=(const rook &r) {
    bitboard::operator=(r);
    return *this;
}

rook &rook::operator=(rook &&r) {
    bitboard::operator=(r);
    return *this;
}
};

#endif //BITBOARD_ROOK_H

