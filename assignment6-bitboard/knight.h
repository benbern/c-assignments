//
// Created by ben on 4/7/16.
//

#ifndef BITBOARD_KNIGHT_H
#define BITBOARD_KNIGHT_H

#include "chesspiece.h"
#include <vector>
#include <cstdint>

class knight : public chesspiece, public bitboard<uint64_t> {
private:
    struct chessVector{//y'know, like a math vector
        int x;
        int y;
        chessVector(int x, int y) : x{x},y{y}{}
    };

    uint64_t chessVectorToMask(const chessVector&);
public:
    std::vector<bitboard<uint64_t> > getAvailableMoves() override;

    //ctors
    knight(const std::string& s);

    knight() = default;
    knight(const knight& k);
    knight& operator=(const knight&);
    knight& operator=(knight&&);
};


std::vector<bitboard<uint64_t> > knight::getAvailableMoves(){

    static chessVector knightMoves[] = {// I could not define this as a private member
            chessVector{1,2},           // variable
            chessVector{-1,2},
            chessVector{-1,-2},
            chessVector{1,-2},
            chessVector{2,1},
            chessVector{2,-1},
            chessVector{-2,1},
            chessVector{-2,-1}
    };


    std::vector<bitboard<uint64_t> > availMoves;
    std::vector<chessVector> pieces;
    auto len = bitboard::getRowCount();

    //for each knight piece
    for(int y{0}; y < len; ++y)
        for(int x{0}; x < len; ++x)
            if(operator[](x + y * len))
                pieces.push_back({x,y});


    for(auto & piece : pieces)
    {
        for(auto & move : knightMoves)
        {
            auto x = move.x + piece.x;
            auto y = move.y + piece.y;

            if (x < len && x >= 0 &&
                y < len && y >= 0 &&
                !operator[](x + y * len))
            {
                auto tempmask = chessVectorToMask({x,y});
                auto piecePos = chessVectorToMask(piece);
                bitboard<uint64_t> tempBB(tempmask);
                bitboard<uint64_t> pieceBB(piecePos);
                availMoves.push_back(operator+(tempBB) - pieceBB);
            }
        }
    }
    //get available moves
    return availMoves;
}

uint64_t knight::chessVectorToMask(const knight::chessVector &cv)
{ return (1uLL << (cv.x + cv.y * bitboard::getRowCount())); }


knight::knight(const std::string &s) : bitboard{s} {}
knight::knight(const knight& k): bitboard{k.board} {}

knight &knight::operator=(const knight &k) {
    bitboard<uint64_t>::operator=(k);
    return *this;
}

knight &knight::operator=(knight &&k) {
    bitboard<uint64_t>::operator=(k);
    return *this;
}

#endif //BITBOARD_KNIGHT_H