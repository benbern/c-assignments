//
// Created by ben on 4/7/16.
//

#ifndef BITBOARD_BITBOARD_H
#define BITBOARD_BITBOARD_H

#include<string>
#include<cstddef>
#include<cstring>
#include<iostream>
#include<algorithm>
#include <stdexcept>
#include <typeinfo>

static const char BITMARKER = 'x';
static const char NOBITMARKER = '-';

template<typename board_type>
class bitboard {
protected:
    enum { CHAR_BITS = 8 };
    board_type board = static_cast<board_type>(0uLL);
public:
    template<class U>
    friend std::ostream &operator<<(std::ostream &o, const bitboard<U> &bb);
    template<class U>
    friend std::istream &operator>>(std::istream &in, bitboard<U> &bb);
    bitboard operator+(const bitboard &rhs) const;
    void operator+=(const bitboard &rhs);
    bitboard operator-(const bitboard &rhs) const;
    void operator-=(const bitboard &rhs);
    bitboard operator&(const bitboard &rhs) const;
    void operator&=(const bitboard &rhs);
    bool operator!=(const bitboard &rhs) const;
    bool operator==(const bitboard &rhs) const;
    bool operator[](int i) const;
    void put(std::size_t i);
    void erase(std::size_t i);
    void invert(std::size_t i);
    void clear();
    bitboard(board_type b);
    bitboard(const std::string &s);
    bitboard(const char *s);
    void setBoard(board_type);
    bitboard() = default;
    bitboard(const bitboard &) = default;
    bitboard &operator=(const bitboard &) = default;
    bitboard(bitboard &&) = default;
    board_type cStrToBoardType(const char *cstr);
    virtual std::size_t getSize() const;
    virtual std::size_t getRowCount() const;
};

template <typename board_type>
board_type bitboard<board_type>::cStrToBoardType(const char *cstr) {
    auto len = std::strlen(cstr);
    board_type return_board = static_cast<board_type>(0uLL);

    for (std::size_t i{0}, newline_offset{0}; i < len; ++i) {
        if (tolower(cstr[i]) == 'x')
            return_board |= (static_cast<board_type>(1uLL) << (i - newline_offset));

        else if (isspace(cstr[i]))
            ++newline_offset;

    }
    return return_board;
}

template<typename board_type>
std::ostream &operator<<(std::ostream &o, const bitboard<board_type> &bb) {
    auto row_size = bb.getRowCount();
    auto len = bb.getSize();

    for (std::size_t i{0}; i < len; ++i) {
        if ((i % row_size) == 0 && i != 0)
            o.put('\n');

        if (bb[i])
            o.put(BITMARKER);
        else
            o.put(NOBITMARKER);
    }
    return o;
}


template<typename board_type>
std::istream &operator>>(std::istream &in, bitboard<board_type> &bb) {
    std::string s;
    in >> s;
    bb.board = bb.cStrToBoardType(s.c_str());
    return in;
}

template<typename board_type>
std::size_t bitboard<board_type>::getSize() const { return sizeof(board_type) * CHAR_BITS; }

template<typename board_type>
std::size_t bitboard<board_type>::getRowCount() const { return CHAR_BITS; }

template<typename board_type>
void bitboard<board_type>::setBoard(board_type b) { this->board = b; }

template<typename board_type>
bitboard<board_type> bitboard<board_type>::operator+(const bitboard &rhs) const {
    return bitboard(this->board | rhs.board);
}

template<typename board_type>
void bitboard<board_type>::operator+=(const bitboard &rhs) { this->board |= rhs.board; }

template<typename board_type>
bitboard<board_type> bitboard<board_type>::operator-(const bitboard &rhs) const {
    bitboard shit(*this);

    for (int i{0}; i < sizeof(rhs.board) * CHAR_BITS; ++i)
        if (rhs.board & (static_cast<board_type>(1uLL) << i) && shit.board & (static_cast<board_type>(1uLL) << i))
            shit.board ^= (static_cast<board_type>(1uLL) << i);

    return shit;
}

template<typename board_type>
void bitboard<board_type>::operator-=(const bitboard<board_type> &rhs) {
    for (int i{0}; i < (sizeof(rhs.board) * CHAR_BITS); ++i)
        if (rhs.board & (static_cast<board_type >(1uLL) << i) && this->board & (static_cast<board_type>(1uLL) << i))
            this->board ^= (static_cast<board_type>(1uLL) << i);
}

template<typename board_type>
bitboard<board_type> bitboard<board_type>::operator&(
        const bitboard<board_type> &rhs) const { return bitboard<board_type>(this->board & rhs.board); }

template<typename board_type>
void bitboard<board_type>::operator&=(const bitboard<board_type> &rhs) { this->board &= rhs.board; }

template<typename board_type>
bool bitboard<board_type>::operator!=(const bitboard<board_type> &rhs) const { return this->board != rhs.board; }

template<typename board_type>
bool bitboard<board_type>::operator==(const bitboard<board_type> &rhs) const { return this->board == rhs.board; }

template<typename board_type>
bool bitboard<board_type>::operator[](int i) const {
    return static_cast<bool>(this->board & (static_cast<board_type>(1uLL) << i));
}

template<typename board_type>
void bitboard<board_type>::put(std::size_t i) { this->board |= (static_cast<board_type>(1uLL) << i); }

template<typename board_type>
void bitboard<board_type>::erase(std::size_t i) { this->board &= ~(1uLL << i); }

template<typename board_type>
void bitboard<board_type>::invert(std::size_t i) { this->board ^= (static_cast<board_type>(1uLL) << i); }

template<typename board_type>
void bitboard<board_type>::clear() { this->board &= static_cast<board_type>(0uLL); }

template<typename board_type>
bitboard<board_type>::bitboard(board_type b) { this->board |= static_cast<board_type>(b); }

template<typename board_type>
bitboard<board_type>::bitboard(const std::string &s) { this->board = cStrToBoardType(s.c_str()); }

template<typename board_type>
bitboard<board_type>::bitboard(const char *s) { this->board = cStrToBoardType(s); }

#endif //BITBOARD_BITBOARD_H
