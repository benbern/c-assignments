//
// Created by ben on 4/7/16.
//


#include"bitboard.h"
#include<iostream>

using namespace std;

int main(int,char **){

    bitboard<unsigned short> bbshort(1);

    cout << "bbshort is " << bbshort.board;

    bitboard<unsigned long long>bb10(10);
    bitboard<unsigned long> bb9(9);
    bitboard<unsigned long> bb1(1);

    cout << "bb9 is " << bb9.board << endl;
    cout << "bb1 is " << bb1.board << endl;

    cout << " the diff of bb9 and bb1 is " << (bb9 - bb1).board << endl;

    bitboard<unsigned long> bb15(bb1);

    bb15.set(1);
    bb15.set(2);
    bb15.set(3);

    cout << " the combination of bb15 and bb9 is now " << (bb15 + bb9).board << endl;
    cout << " the combination of bb1 and bb15 is now " << (bb1 + bb15).board << endl;
    cout << " the combination of bb1 and bb9 is now " << (bb1 + bb9).board << endl;

    bb9 -= bb1;

    cout << "bb9 should now be 8, and it's  " << bb9.board << endl;

    const char * board_str_state = "x--xx---\n--------\n--------\n--------";

    bitboard<unsigned long> cstr_test_b(board_str_state);

    cout << "the cstr tes b results are " << cstr_test_b.board << endl;


    return 0;
}

/*
 *
 */

