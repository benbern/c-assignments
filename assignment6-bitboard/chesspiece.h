//
// Created by ben on 4/7/16.
//

#ifndef BITBOARD_CHESSPIECE_H
#define BITBOARD_CHESSPIECE_H

#include "bitboard.h"
#include <vector>
#include <cstdint>

class chesspiece{
public:
    virtual std::vector<bitboard<uint64_t> > getAvailableMoves() = 0;
};




#endif //BITBOARD_CHESSPIECE_H
