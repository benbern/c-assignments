//
// Created by ben on 3/1/16.
//
/*
 * summary of things I learned.
 * I learned that I should jest get the program running and then turn it in
 * and not worry so much about getting user input
 */

#if defined(_WIN32)
#define listcmd "dir /b *.dat > datfiles.txt 2>nul"
#elif defined(__GNUC__)
#define listcmd "ls *.dat > datfiles.txt 2>/dev/null"
#else
#error Unsupported compiler.
#endif

#include <algorithm>
#include <numeric>
#include <iterator>
#include <fstream>
#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <bitset>
#include <map>
#include <string>

using namespace std;

static const u_int8_t PARAM_N = 5;
static const char COMMENT_DELIM = '#';
enum FLAG {
    VT = 0, WIDTH = 1, PULSE_DELTA = 2, DROP_RATIO, BELOW_DROP_RATIO
};
const std::string PARAMS[PARAM_N] = {"vt", "width", "pulse_delta", "drop_ratio", "below_drop_ratio"};

/* I copied the following three std::string -> std::string functions from Stack Overflow
 * Source: http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
 */
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

struct Config {
    int vt;
    int width;
    int pulse_delta;
    double drop_ratio;
    int below_drop_ratio;
};

Config createConfig(std::istream &in) {
    Config conf;
    bitset<PARAM_N> flag;
    string input_line;
    vector<std::string> tmp_v;

    string temp;

    while (getline(in, temp))
        tmp_v.push_back(temp);

    transform(begin(tmp_v), end(tmp_v), begin(tmp_v), trim);
    for_each(begin(tmp_v), end(tmp_v), [&](std::string &line) {
        if (line[0] != COMMENT_DELIM && line.size() > 0) {
            for_each(begin(line), end(line), [](char &c) { tolower(c); });
            auto pos_of_eq = line.find('=');
            string param = line.substr(0, pos_of_eq);
            param = trim(param);
            auto val = line.substr(pos_of_eq + 1, line.size());
            val = trim(val);
            if (param == PARAMS[VT]) {
                if (flag[VT]) throw runtime_error("Duplicate Param: " + PARAMS[VT]);

                conf.vt = stoi(val);
                flag.set(VT);
            }
            else if (param == PARAMS[WIDTH]) {
                if (flag[WIDTH]) throw std::runtime_error("Duplicate Param: " + PARAMS[WIDTH]);

                conf.width = stoi(val);
                flag.set(WIDTH);
            }
            else if (param == PARAMS[PULSE_DELTA]) {
                if (flag[PULSE_DELTA]) throw std::runtime_error("Duplicate Param: " + PARAMS[PULSE_DELTA]);

                conf.pulse_delta = stoi(val);
                flag.set(PULSE_DELTA);
            }
            else if (param == PARAMS[DROP_RATIO]) {
                if (flag[DROP_RATIO]) throw runtime_error("Duplicate Param: " + PARAMS[DROP_RATIO]);

                conf.drop_ratio = stod(val);
                flag.set(DROP_RATIO);
            }
            else if (param == PARAMS[BELOW_DROP_RATIO]) {
                if (flag[BELOW_DROP_RATIO]) throw runtime_error("Duplicate Param: " + PARAMS[BELOW_DROP_RATIO]);

                conf.below_drop_ratio = std::stoi(val);
                flag.set(BELOW_DROP_RATIO);
            }
            else {
                throw runtime_error("Unknown param: " + param);
            }
        }
    });
    if (!flag.all())
        throw runtime_error("Error: missing parameters in ini file");

    return conf;
}

enum SMOOTHFACTOR {
    TWO = 2, THREE = 3, FOUR = 4, DENOM = 15
};

int main(int argc, char **argv) {
    if (argc == 1) {
        cout << "Usage: ./a.out <config_file.ini>\n";
        exit(1);
    }

    Config conf;

    try {
        ifstream configfile(argv[1]);

        if (!configfile.good()) {
            string err_msg("Config file: ");
            throw runtime_error(err_msg + argv[1] + " cannot be found.");
        }

        conf = createConfig(configfile);

    } catch (runtime_error &err) {
        cout << err.what();
        exit(1);
    }

    system(listcmd);
    ifstream datfiles("datfiles.txt");
    vector<string> file_names((istream_iterator<string>(datfiles)), istream_iterator<string>());

    for_each(begin(file_names), end(file_names), [&conf](string &file_name) {
        ifstream in(file_name.c_str());
        vector<int> data((istream_iterator<int>(in)), istream_iterator<int>());
        transform(begin(data), end(data), begin(data), negate<int>());
        vector<int> smoothed_data{begin(data), begin(data) + THREE};

        transform(begin(data) + THREE, end(data) - THREE, back_inserter(smoothed_data), [](int &datum){
            return (*(&datum - THREE) + TWO * *(&datum - TWO) + THREE * *(&datum - 1) + THREE * datum +
                    THREE * *(&datum + 1) + TWO * *(&datum + TWO) + *(&datum + THREE)) / DENOM;
        });

        copy(end(data) - THREE, end(data), back_inserter(smoothed_data));

        vector<ptrdiff_t> pulses;

        for (auto iter = begin(smoothed_data); iter != end(smoothed_data) - 2; ++iter) {
            if (iter[2] - iter[0] > conf.vt) {
                auto pulse_start = iter;

                auto pulse_end = adjacent_find(pulse_start + 2, end(smoothed_data),
                                               greater<int>());

                pulses.push_back(pulse_start - begin(smoothed_data));
                iter = pulse_end - 1;
            }
        }


        for (auto iter = begin(pulses); iter != end(pulses); ++iter) {

            if (iter + 1 != end(pulses) && iter[1] - iter[0] <= conf.pulse_delta) {

                auto pulse_end = adjacent_find(begin(smoothed_data) + iter[0] + TWO, end(smoothed_data),
                                               greater<int>());

                auto peak = max_element(begin(smoothed_data) + iter[0], pulse_end);

                //get number of points less than drop_ratio times the height of the peak of the first pulse
                auto num_points_lt = count_if(peak + 1, begin(smoothed_data) + iter[1], [&conf, &peak](int datum) {
                    return datum < conf.drop_ratio * peak[0];
                });

                if (num_points_lt > conf.below_drop_ratio) {
                    pulses.erase(remove(begin(pulses), end(pulses), iter[0]), end(pulses));
                    iter--;
                }
            }
        }

        cout << file_name << " ";

        copy(begin(pulses), end(pulses), ostream_iterator<ptrdiff_t>(cout, " "));

        cout << endl;

        for (auto itr = begin(pulses); itr != end(pulses); ++itr) {
            ptrdiff_t end_delim_offset;

            if (itr + 1 != end(pulses))
                end_delim_offset = (itr[1] - itr[0]) < conf.width ? (itr[1] - itr[0]) : conf.width;
            else
                end_delim_offset = conf.width;

            if (begin(data) + itr[0] + end_delim_offset > end(data))
                end_delim_offset = end(data) - begin(data) - itr[0];

            auto area = accumulate(begin(data) + itr[0], begin(data) + itr[0] + end_delim_offset, 0);

            cout << area << " ";
        }


        cout << endl;
    });

    return 0;
}