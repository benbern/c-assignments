//
// deque.h
// This is an implementation of a deque data structure
// Created by Ben Berntson on 1/8/16.
//
//

/*
 * SUMMARY OF WHAT I LEARNED AND WHAT PROBLEMS I HAD TO
 * OVERCOME:
 *
 * To be honest, I did not learn anything directly from this
 * program. However, I did download the CPPUnit testing framework
 * and I initially developed this using that. I then installed
 * gTest and played around with that. So I learned a little
 * bit about unit testing frameworks for C++.
 *
 * I did not have any problems when writing this deque.
 *
 */

#ifndef DEQUE_H
#define DEQUE_H

#include<cstddef>
#include<algorithm>
#include<stdexcept>
#include<cassert>

template<typename T>

class Deque
{
    enum { CHUNK = 10 };

private:
    T* _front;
    T* _back;
    T* _arr;
    size_t _capacity;

    /**
     * grow
     *
     * grow is an internal routine that grows the array
     * and adds half of CHUNK to each end of
     * the array for extra buffer space
     *
     */
    void grow()
    {
        //increase capacity size
        _capacity += CHUNK;

        //allocate a new array w/new capacity size
        T* tempArr = new T[ _capacity ];

        //copy over and set back pointer
        _back = std::copy( _front, _back, tempArr + CHUNK/2 );

        //delete previous arr
        delete [] _arr;

        //set arr to temp
        _arr = tempArr;

        //set _front to correct position
        _front = ( _arr + CHUNK/2  );
    }


public:

    /**
     * Deque default constructor
     *
     * the default constrsuctor initializes the Deque object's
     * capacity to its correct size using CHUNK
     *
     */
    Deque() : _capacity{ CHUNK }
    {
        // Allocate default-size array
        _arr = new T[ CHUNK ];
        _front = _back = _arr + CHUNK / 2;
    }

    /**
     * destructor
     *
     * deletes any memory allocated to arr
     *
     */
    ~Deque()
    {
        delete[] _arr;
    }


    /**
     * push_front
     *
     * Inserts an elment info the front of the deque
     * checks to see if there is room and resizes properly
     *
     */
    void push_front(T element)
    {

        if( _front == _arr )
        {
            grow();
            assert( _front < _back );
        }

        --_front;
        *_front = element;

    }

    /**
     * push_back
     *
     * Inserts an elment into the back end of the deque
     *
     */
    void push_back(T element)
    {
        //check to see if _back is near the end of array
        if( _back == _arr + _capacity - 1  )
        {
            grow();
            assert( _back > _front);
        }

        *_back  = element;
        ++_back;
    }

    /**
     * front
     *
     * returns a reference to the first used element
     *
     */
    T& front()
    {
        if( _front == _back )
            throw std::logic_error( "deque is empty") ;

        return *_front;
    };

    /**
     * back
     *
     * Returns a reference to the last used element
     */
    T& back()
    {
        if( _front == _back )
            throw std::logic_error( "deque is empty" );

        return *( _back - 1 );
    };

    /**
     * at
     *
     * Returns a reference to the element pos by reference
     *
     */
    T& at(size_t pos)
    {
        if( _front == _back )
            throw std::logic_error( "deque is empty" );

        return *( _front + pos );
    }

    /**
     * pop_front
     *
     * "Removes" first deque element (just change _front)
     */
    void pop_front()
    {
        //tests to see if there's an element
        if( _front < _back )
        { _front++; }
    }

    /**
     * pop_back
     *
     * "Removes" last deque element (just changes _back)
     */
    void pop_back()
    {
        //test to see if there's an element
        if( _back > _front )
        { _back--; }

    }

    /**
     * size
     *
     * Returns the number of used items.
     */
    size_t size() const
    {
        assert( _back >= _front );

        return ( _back - _front );
    }

    /**
     * begin
     *
     * Returns a pointer to the first element
     *
     */
    T* begin() { return _front; }

    /**
     * end()
     *
     * Returns a pointer to one position past the last element
     */
    T* end() { return _back; }

};


#endif 
